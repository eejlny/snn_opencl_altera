This SNN (Spiking Neural Network) has been tested in the DE1 SOC board with the ARM processor as host.

To get going do:

compile host application:

start SOC EDS Altera suite (so the compiler tools for the ARM processor are available) go the top directory and type make. This will do 
$ make
arm-linux-gnueabihf-g++  host/src/main.cpp  host/src/Helpers.cpp  host/src/CL
p  -o neuron  -IC:/programs/altera/15.0/hld/host/include  -O3 -I./common/inc
ost/inc -LC:\programs\altera\15.0\hld\board\terasic\de1soc\arm32\lib -LC:/pro
ms/altera/15.0/hld/host/arm32/lib -Wl,--no-as-needed -lalteracl  -lalterahalm
-lalterammdpcie -lelf -lrt -ldl -lstdc++

and you will the neuron executable

Then if you want a new aocx make sure the Altera opencl compiler aoc is available and then from the top directory do:

aoc -O3 --fpc --fp -relaxed -Daltera -v --report ./device/neuron.cl -o ./bin/neuron.aocx

and then in the bin directory you will get your aocx that the host application will use.